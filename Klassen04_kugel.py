import math

class Kugel():
    def __init__(self,radius):
        self.radius = radius

    def surface(self):
        print(4 * math.pi * self.radius **2)

    def volume(self):
        print(4 / 3 * math.pi * self.radius ** 3)

k = Kugel(4)
k.surface()
k.volume()

