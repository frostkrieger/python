print("Tic-Tac-Toe Python Tutorial")
spielfeld = ["","1","2","3",
             "4","5","6",
             "7","8","9"]

spiel_aktiv = True

def spielfeld_ausgeben():
    print (spielfeld[1] + "|" + spielfeld[2] + "|" + spielfeld[3])
    print (spielfeld[4] + "|" + spielfeld[5] + "|" + spielfeld[6])
    print (spielfeld[7] + "|" + spielfeld[8] + "|" + spielfeld[9])
spielfeld_ausgeben()

##Spielereingabe und Kontrolle der Eingabe
def spieler_eingabe():
    global spiel_aktiv
    while True:
        spielzug = input("Bitte Feld Eingeben: ")
        if spielzug == "q":
            spiel_aktiv = False
            return()
        try:
            spielzug = int(spielzug)
        except ValueError:
            print("Bitte eine Zahle von 1 bis 9 eingeben")
        else:
            if spielzug >=1 and spielzug <=9:
                return spielzug
            else:
                print("Zahl muss zwischen 1 und 9 liegen")
spielzug=spieler_eingabe()
print("Spielzug: " + str(spielzug))

##Hauptteil




