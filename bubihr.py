import csv

with open('users_exit_bubihr.csv', newline='', encoding='cp1252') as csvfile:
    bubihrreader = csv.reader(csvfile, delimiter=';', quotechar='"')
    
    for row in csvfile:
        splitted = str(row.strip()).split(";")
        
        ausgeschieden = splitted[2]
        vorname = splitted[0]
        name = splitted[1]
        
        count = 0
        if ausgeschieden != "NULL":
            new_list = [vorname, name, ausgeschieden]
            for ele in new_list:
                new_list[count]=ele[1:-1]
                count += 1
            print(new_list)