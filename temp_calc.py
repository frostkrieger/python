#Geg.: Temperatur in Grad Celsius, c
#Ges.: Temperatur in Grad Kelvin, k
#k = c + 273,15


def auswahl():
    a = input("Wählen sie zum umrechnen Celsius in eine andere Temperatur wie folgt: 1 für Kelvin, 2 für Fahrenheit, 3 für Rankine, 4 für Reamour")
    return int(a)

def get_temp():
    while True:
        c = input("Gib die Temperatur in Grad Celsius an: ")
        try:
            c = float(c)
            return c
        except ValueError:
            print("Das ist keine gültige Temperatur")

def kelvin(c):    
    k = c + 273.15
    return k

def fahrenheit(c):
    f = c*1.8+32
    return f

def rankine(c):
    r = c*1.8+32+459.67
    return r

def reamour(c):
    ra = c*0.8
    return ra

aw = auswahl()
c = get_temp()
if aw == 1:
    print("Die Temperatur in Kelvin beträgt:" + str(kelvin(c)) + " Kelvin")
elif aw == 2:
    print("Die Temperatur in fahrenheit beträgt:" + str(fahrenheit(c)) + " fahrenheit")
elif aw == 3:
    print("Die Temperatur in rankine beträgt:" + str(rankine(c)) + " rankine")
elif aw == 4:
    print("Die Temperatur in reamour beträgt:" + str(reamour(c)) + " reamour")








