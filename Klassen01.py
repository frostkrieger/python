class Car():
    def __init__(self, farbe, ps, tueren, laenge, breite, hoehe, verbrauch, alter, marke, kmstand):
        self.farbe = farbe
        self.ps = ps
        self.tueren = tueren
        self.laenge = laenge
        self.breite = breite
        self.hoehe = hoehe
        self.verbrauch = verbrauch
        self.alter = alter
        self.marke = marke
        self.kmstand = kmstand

    def hupen(self, anzahl = 1):
        print(anzahl * "Nööööt Nööööööt ")
    
    def rasen(self, geschwindigkeit):
        print("Der", self.marke, "macht", geschwindigkeit, "km/h")

    def fahren(self, km):
        self.kmstand = self.kmstand + km 
        print("Der ", self.marke, " ist ", km, " km gefahren")
        print("Der", self.marke, "ist",(self.kmstand), " gefahren")
    
    def parken(self):
        print("Parkplatz gefunden")
    
    def kilometerstand(self):
        print("Ich habe", str(self.kmstand), "auf dem Tacho")


auto01 = Car("Schwarz","740PS","2","2.5m","1,4m","1.2m","7L/100km","3 Jahre","Ferrari",5000)
auto01.rasen(345)
auto01.hupen()
auto01.fahren(32)




