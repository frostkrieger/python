class Cube():
    
    def __init__(self, seitenlaenge):
        self.seitenlaenge = seitenlaenge
    
    def volume(self):
        print(self.seitenlaenge ** 2 * 6) ## **=Quadrat

    def surface(self):
        print(self.seitenlaenge **3)

a = Cube(3)
a.volume()
a.surface()

