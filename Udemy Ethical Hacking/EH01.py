import hashlib

password_hash = "112aa01926aebb65c5e09cc0a25ce2b5cff2ec5df0e9b123510db6753557e552"
sonderzeichen = "!$%&/()=?"

with open("dictionary.txt", ("r")) as file:
    for line in file:
        passwort = line.strip()
        
        for zeichen in sonderzeichen:
            for zeichen2 in sonderzeichen:
                pw = passwort + zeichen + zeichen2
        
                if hashlib.sha256(pw.encode()).hexdigest() == password_hash:
                    print("Das Passwort lautet: " + pw)
