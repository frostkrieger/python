import csv

names = {}

with open("names.csv", newline="") as csvfile:
    namereader = csv.reader(csvfile, delimiter=',', quotechar='"')
    counter = 0
    for line in namereader:
        if counter != 0:
            anzahl = int(line[5])
            name = line[1]
            
            if name in names:
                names[name] = names[name] + anzahl
            else:
                names[name] = anzahl
        counter = counter + 1

import queue

pq = queue.PriorityQueue()

for name, anzahl in names.items():
    pq.put((-anzahl, name))
for i in range(0, 5):
    print(pq.get())


