
##Konstanten
K1 = 1.8
K2 = 32
K3 = -459.67
K4 = -273.15

#Methode für die Eingabe der Temperatur die umgewandelt werden soll
#und prüfung ob die Eingabe ok ist.

def temp_eingabe(msg = "Bitte geben Sie die Temperatur ein die Sie umwandel wollen: "):
        while True:
            try:
                temp = float(input(msg))
                return temp
            except ValueError:
                print("Mach erscht a ma die Baischiks! Gib ne Zahl ein! ") 

##Methoden für die Berechnung der Temperaturen

def cel_kel(t):
    if t >= K4:
        t = t - K4
        return t
    else:
        print("Bischt du behindat?! Die Temperatur kann nischt < -273,15 schein!")

   
def cel_fah(t):
    if t >= K4:
        t = t * K1 + K2
        return t
    else:
        print("Bischt du behindat?! Die Temperatur kann nischt < -273,15 schein!")


def kel_cel(t):
    if t >= 0:
        t = t + K4
        return t
    else:
        print("Bischt du behindat?! Die Temperatur kann nischt < -273,15 schein!")


def kel_fah(t):
    if t >= 0:
        t = (t - K4)*K1 + K2
        return t
    else:
        print("Bischt du behindat?! Die Temperatur kann nischt < -273,15 schein!")



def fah_cel(t):
    if t >= K3:
        t = (t - K2) / K1
        return t
    else:
        print("Bischt du behindat?! Die Temperatur kann nischt < -273,15 schein!")

def fah_kel(t):
    if t >= K3:
        t= (t + K3) / K1
        return t
    else:
        print("Bischt du behindat?! Die Temperatur kann nischt < -273,15 schein!")

auswahl = 0
while auswahl != "q":
    print("(1) Umrechnung von Celsius nach Kelvin")
    print("(2) Umrechnung von Celsius nach Fahrenheit")
    print("(3) Umrechnung von Kelvin nach Celsius")
    print("(4) Umrechnung von Kelvin nach Fahrenheit")
    print("(5) Umrechnung von Fahrenheit nach Celsius")
    print("(6) Umrechnung von Fahrenheit nach Kelvin")
    auswahl = input("Bitte wählen Sie eine Umrechnung aus: ")

    if auswahl == "1":
        t = temp_eingabe() ## Hier wird die msg aus der Methode temp_eingabe übergeben, man kann hier auch 
                        ## selber was reinschreiben, dann wird der Wert aus msg überschrieben
        print(t, "C° = ", cel_kel(t), "K", sep = "")

    elif auswahl == "2":
        t = temp_eingabe()
        print(t, "C° = ", cel_fah(t), "F", sep = "")

    elif auswahl == "3":
        t = temp_eingabe()
        print(t, "K° = ", kel_cel(t), "C", sep = "")

    elif auswahl == "4":
        t = temp_eingabe()
        print(t, "K° = ", kel_fah(t), "F", sep = "")

    elif auswahl == "5":
        t = temp_eingabe()
        print(t, "F = ", fah_cel(t), "C°", sep = "")

    elif auswahl == "6":
        t = temp_eingabe
        print(t, "F = ", fah_kel(t), "K", sep = "")
    else:
        print("Programm beendet")
        break





