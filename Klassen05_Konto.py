class Konto():
    def __init__(self, kontostand, pin):
        self.kontostand = kontostand
        self.pin = pin

    def anzeige(self):
        print(self.kontostand)
    
    def einzahlen(self, geld):
        self.kontostand += geld

    def abheben(self, geld, pin):
        if self.pin == pin:
            if self.kontostand >= geld:
                self.kontostand -= geld
            else:
                print("Sie können nur noch", str(self.kontostand), "€ abheben!")
        else:
            print("Sie haben die falsche Pin eingeben!")

Kunde1337 = Konto(500, "1337")
Kunde1337.anzeige()
Kunde1337.einzahlen(40)
Kunde1337.anzeige()
Kunde1337.abheben(25, "1337")
Kunde1337.anzeige()
Kunde1337.abheben(300, "1337")
Kunde1337.anzeige()


