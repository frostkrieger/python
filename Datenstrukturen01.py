import csv

names = set()

with open('names.csv', newline='') as csvfile:
    namereader = csv.reader(csvfile, delimiter=',', quotechar='"')
    counter = 0
    for row in namereader:
        if counter != 0:
            names.add(row[1])
        counter = counter + 1
print(len(names))

