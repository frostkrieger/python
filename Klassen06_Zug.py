class Zug():
    def __init__(self, route, position): ## route = Liste wird eingelesen, position= position in der Liste
        self.route = route
        self.position = position

    def show_station(self):
        print(self.route[self.position])

    def move(self):
        if self.position < len((self.route)) - 1:
            self.position += 1
        else:
            print("Endstation, alle Aussteigen! Nöööööt Nööööööööt")
    
    def move_back(self):
        if self.position > 0:
            self.position -= 1
        else:
            print("Du bist schon im Bahnhof")
    
    def bypass_station(self, station):
        if station in self.route:
            self.route.remove(station)
            self.station = 0

ice = Zug(["Paris","Budapest","Bukarest","Istanbul"], 0)
ice.bypass_station("Budapest")
ice.move()
ice.show_station()