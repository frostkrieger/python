print("(1) Umrechnung von Celsius nach Kelvin")
print("(2) Umrechnung von Celsius nach Fahrenheit")
print("(3) Umrechnung von Kelvin nach Celsius")
print("(4) Umrechnung von Kelvin nach Fahrenheit")
print("(5) Umrechnung von Fahrenheit nach Celsius")
print("(6) Umrechnung von Fahrenheit nach Kelvin")
print("(q) Beendet das Programm")

kel = -273.15   

auswahl = input("Wählen Sie eine Zahl zwischen 1 und 6 aus dem Menü aus: ")

if auswahl == "1":
    celsius1 = float(input("Wählen Sie die Temperatur: "))
    resultkel1 = celsius1 + kel
    print("Das Ergebnis lautet: " + str(resultkel1) + " K")

elif auswahl == "2":
    celsius2 = float(input("Wählen Sie die Temperatur: "))
    resultfah1 = celsius2 *1.8 + 32
    print("Das Ergebnis lautet: " + str(resultfah1) + " F")

elif auswahl == "3":
    kelvin1 = float(input("Wählen Sie die Temperatur: "))
    resultcel1 = kelvin1 - kel
    print("Das Ergebnis lautet: " + str(resultcel1) + " °C")

elif auswahl == "4":
    kelvin2 = float(input("Wählen Sie die Temperatur: "))
    resultfah2 = (kelvin2 - kel) * 1.8 + 32
    print("Das Ergebnis lautet: " + str(resultfah2) + " F")

elif auswahl == "5":
    fahrenheit1 = float(input("Wählen Sie die Temperatur: "))
    resultcel2 = (fahrenheit1 - 32) / 1.8
    print("Das Ergebnis lautet: " + str(resultcel2) + " °C")

elif auswahl == "6":
    fahrenheit2 = float(input("Wählen Sie die Temperatur: "))
    resultkel2 = (fahrenheit2 + 459.67) / 1.8
    print("Das Ergebnis lautet: " + str(resultkel2) + " K")
else:
    auswahl = "q"
    print("Sie haben das Programm beendet")

    