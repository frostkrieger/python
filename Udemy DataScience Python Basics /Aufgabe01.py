##Aufgabe1
print(7 ** 4)

##Aufgabe2
s = "Hallo lieber Sam!"
print(s.split())

##Aufgabe3
planet = "Erde"
durchschnitt = 12742

print("Der Durchschnitt der {} beträgt {} Kilometer".format(planet, durchschnitt))

##Aufgabe4
lst = [1,2,[3,4],[5,[100,200,['Hallo']],23,11],1,7]
lst[3][1][2][0]