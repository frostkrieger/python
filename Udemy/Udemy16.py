class Account():
    def __init__(self, credits, pin):
        self.credits = credits
        self.pin = pin

    def display(self):
        print(self.credits)

    def pay_in(self, money):
        self.credits += money
    
    def withdraw(self, money, pin):
        if self.pin == pin:
            if self.credits >= money:
                self.credits -= money
            else:
                print("Du kannst nur noch " + str(self.credits) + "€ abheben!")
        else:
            print("Du hast die Falsche Pin eingegeben")

Kunde01 = Account(500, "1234")
Kunde01.display()
Kunde01.pay_in(40)
Kunde01.display()
Kunde01.withdraw(25, "1234")
Kunde01.display()
Kunde01.withdraw(600, "1234")

