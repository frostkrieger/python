##Ermittle den 5.-häufigsten Vornamen, der in den USA vergeben wurde! Lies dazu die ../data/names.csv - Datei ein.
#Verwende dazu zuerst ein Dictionary, mit dem du die Häufigkeit der Vornamen zählst und anschließend eine PriorityQueue,
#um die Top 5 Vornamen zu ermitteln.

import csv
import queue
names = {}

with open("names.csv", newline='') as csvfile:
     namereader = csv.reader(csvfile, delimiter=',', quotechar='"')
     counter = 0
     for line in namereader:
         if counter != 0:
             number = int(line[5])
             name = line[1]

             if name in names:
                 names[name] = names[name] + number
             else:
                 names[name] = number
         counter = counter + 1

pq = queue.PriorityQueue()

for name, number in names.items():
     pq.put((-number, name))
for i in range(0, 5):
     print(pq.get())




