##Finde heraus, wie oft der Name "Max" als männlicher Vorname in Kalifornien zwischen 1950 und 2000 (jeweils einschließlich) vergeben wurde!
##Verwende dazu die bereitgestellte .csv - Datei (../data/names.csv)!

anzahl = 0

with open("names.csv", "r") as file:
    for line in file:
        splitted = line.strip().split(",")
        if splitted[2] == "Year":
            continue 
        jahr = int(splitted[2])

        if splitted[1] == "Max" and jahr >= 1950 and jahr <= 2000 and splitted[3] == "M" and splitted[4] == "CA":
            anzahl = anzahl + int(splitted[5])
    print(anzahl)
        