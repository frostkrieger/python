##Aufgabe:

##Lese die ../data/names.csv - Datei ein und berechne, welcher Name insgesamt in den gesamten USA am häufigsten vergeben wurde.

##Tipps:

##Lies zuerst die Daten in ein Dictionary ein und zähle, wie oft jeder Vorname insgesamt vorgekommen ist.
##Analysiere dann erst das Dictionary und finde den häufigsten Vornamen heraus.
##Achte drauf, wenn du 2 Zahlen addieren möchtest, musst du ggf. einen String zuerst in eine Zahl umwandeln.
##Schreibe den gesamten Code, der die Datei öffnet und durchgeht, in einer Zelle.

dictionary = {}
with open("names.csv", "r") as file:
    for line in file:
        splitted = line.strip().split(",")
        if splitted[0] == "Id":
            continue

        name = splitted[1]                                  ## Namen Filter
        count= int(splitted[5])                             ## Anzahl Filtern

        if name in dictionary:
            dictionary[name] = dictionary[name] + count     ##dictionary bauen Key, Value   
        else:
            dictionary[name] = count
    
    anzahl = 0
    name = ""

    for key, value in dictionary.items():
        if anzahl < value:
            anzahl = value
            name = key
    print(name)



        

