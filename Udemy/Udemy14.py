#Erstelle eine Klasse Cube, mit der du einen Würfel modellierst!
#Die Würfel-Klasse soll als Eigenschaft die Länge einer Würfel-Seite besitzen. 
#Darüber hinaus soll die Klasse auch zwei Methoden haben:
#die Methode volume() berechnet das Volumen und gibt es aus, die Methode surface() berechnet die Oberfläche und gibt sie aus.

class Cube():
    def __init__(self, side):
        self.side = side

    def volumen(self):
        print(self.side **3)

    def oberflaeche(self):
        print(self.side **2 *6)

a = Cube(3)
a.oberflaeche()
a.volumen()
