class Filereader():
    def __init__(self, filename):
        self.filename = filename


    def lines(self):
        lines = []
        with open(self.filename, "r") as file:
            for line in file:
                lines.append(line.strip())
        return lines

class CsvReader(Filereader):                                ##erbt alles von der Klasse Filereader
    def __init__(self,filename):
        super(). __init__(filename)
    
    def lines(self):                                        ##Funktion erbt die in Filereader formatierte Liste lines
        lines = super().lines()
        return [line.split(",") for line in lines]          ##List Comprehension + weitere Bearbeitung der Liste Lines

f = Filereader("./names.csv")
print(f.lines())





